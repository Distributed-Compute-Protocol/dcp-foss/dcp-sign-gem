require 'json'

class DCPSign
  def self.login_message(key, token, application, error)
    if application.proxy == '' || application.proxy.nil?
      endpoint = ENV['PROXY_URL']
    else
      endpoint = application.proxy
    end

    body = {
      oAuthToken: token.token,
      expiry: token.expires_at.to_i,
      endpoint: endpoint
    }

    message_string = {
      owner: key.address.to_s,
      signature: sign(key, body),
      body: body
    }.to_json

    message = {signedMessage: message_string}

    response = HTTParty.post(endpoint, body: message)
    reply_body = response.body
    reply = JSON.parse reply_body

    unless reply['success']
      error "Proxy service returned error: #{reply['error']}"
      error "Error stack: #{reply['stack']}"
      error "Message was: #{message}"
      raise "Error during sending a message to the login service for DCP - #{reply['name']}: #{reply['message']}"
    end

    return reply
  end
  
  def self.send_deposit(transaction, error, my_logger)
    key = Eth::Key.new priv: ENV['TX_KEY']

    body = {
      operation: 'depositFex',
      endpoint: ENV['EXCHANGE_URL'],
      bankAccount: transaction[:bank_account],
      amount: transaction[:amount_usd],
      dccAmount: transaction[:amount_credits],
      exchangeRate: 1/'0.00031706'.to_d,
      currency: 'USD',
      braintreeTxn: transaction[:braintree_id],
      opaque: ''
    }
    
    message_string = {
      owner: key.address.to_s,
      signature: sign(key, body),
      body: body
    }.to_json

    message = {signedMessage: message_string}

    response = HTTParty.post(ENV['EXCHANGE_URL'], body: message)
    reply_body = response.body
    reply = JSON.parse reply_body
    
    unless reply['success']
      error "Exchange service returned error #{reply['error']}"
    else
      transaction[:closed] = Time.now
      transaction.save
      my_logger.info to_tlog
    end
    return reply
  end
  
  def self.revoke_deposit(transaction, error, my_logger)
    key = Eth::Key.new priv: ENV['TX_KEY']

    body = {
      operation: 'revokeFex',
      endpoint: ENV['EXCHANGE_URL'],
      bankAccount: '0x81DaB92d09206666f215913Dad4e50a0246836Ea',
      braintreeTxn: transaction[:braintree_id],
      opaque: ''
    }
    
    message_string = {
      owner: key.address.to_s,
      signature: sign(key, body),
      body: body
    }.to_json

    message = {signedMessage: message_string}
    
    response = HTTParty.post(ENV['EXCHANGE_URL'], body: message)
    reply_body = response.body
    reply = JSON.parse reply_body
     
    unless reply['success']
      error "Exchange service returned error #{reply['error']}"
    else
      transaction[:revoked_at] = Time.now
      transaction.save
      my_logger.info to_tlog
    end
    return reply
  end
  
  def self.to_tlog
    time = self[:updated_at].strftime "%Q\r%b %e %T"
    operation = self[:revoked_at].nil? ? 'deposit' : 'revoke'
    key = Eth::Key.new priv: ENV['TX_KEY']
    "#{time}\toper:#{operation}\taccount:#{key.address}\tamount:#{self[:amount_usd]}\texchangerate:#{1/'0.00031706'.to_d}\ttaxrate:#{self[:tax_rate]}\ttxn:#{self[:braintree_id]}\tuser:#{self[:user_id]}\tip:#{self[:ip]}\n" 
  end

  def self.sign
    sig = key.personal_sign body.to_json
    bin_sig = Eth::Util.hex_to_bin(sig).bytes.rotate(-1).pack('c*')
    {
      r: {
        type: "Buffer",
        data: bin_sig[1..32].bytes
      },
      s: {
        type: "Buffer",
        data: bin_sig[33..65].bytes
      },
      v: bin_sig[0].bytes[0]
    }
  end
end
