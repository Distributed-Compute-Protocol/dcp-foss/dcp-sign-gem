Gem::Specification.new do |s|
  s.name        = "dcp-sign"
  s.version     = "0.0.0"
  s.summary     = "DCP Signing Gem"
  s.description = "Utility for signing DCP messages"
  s.authors     = ["Alex Huctwith"]
  s.email       = "alex@distributive.network"
  s.files       = ["lib/dcp-sign.rb"]
  s.homepage    =
    "https://distributive.network/"
  s.license       = "MIT"
  s.add_runtime_dependency "httparty", [">= 0.21.0"]
  s.add_runtime_dependency "json", [">= 2.7.0"]
  s.add_runtime_dependency "multi_xml", [">= 0.5.2"]
  s.add_runtime_dependency "eth", [">= 0.5.11"]
end
