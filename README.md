# DCP-Sign Gem
This Ruby Gem is used as a utility in DCP-Auth to sign various messages to the DCP system. 
It is capable of handling generic DCP message signing as well.

## Authors and acknowledgment
Alex Huctwith

## License
MIT License
